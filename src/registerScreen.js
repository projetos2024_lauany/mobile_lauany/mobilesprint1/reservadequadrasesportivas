import React from "react";
import { View, TextInput, Button, StyleSheet, Text } from "react-native";


const RegisterScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.TextInput}> INSIRA SUAS INFORMAÇÕES DE CADASTRO </Text>
      <TextInput style={styles.input} placeholder="Nome" />
      <TextInput style={styles.input} placeholder="Email" />
      <TextInput style={styles.input} placeholder="Telefone" />
      <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true} />
      <View style={styles.buttonContainer}>
        <Button style={styles.button} title="CADASTRAR" onPress={() => navigation.navigate('Home')} color='#3e945a' />
      </View>
      <View style={styles.goBackContainer}>
        <Button title="VOLTAR PARA A PÁGINA INICIAL" onPress={() => navigation.navigate('PaginaInicial')} color='#3e945a' />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#faf2f2',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  TextInput: {
    height: 20,
    fontSize: 14, // tamanho da letra
    fontWeight: 'bold', // texto em negrito
    marginBottom: 20,
    marginBottom: 20,
  },
  input: {
    borderColor: '#4dbf72',
    borderWidth: 3,
    borderRadius: 20,
    fontSize: 12, //tamanho da letra de dentro do container 
    width: '80%',
    padding: 10,
    marginVertical: 3, //pra deixar os botões separados
    paddingHorizontal: 10,
    height: 40,
  },
  buttonContainer: {
    backgroundColor: '#e6e6e6',
    borderRadius: 20,
    overflow: 'hidden',
    width: '80%', //  para reduzir o tamanho
    marginBottom: 3,
    marginVertical: 15,
    marginTop: 8,
    
    
  },
  goBackContainer: {
    backgroundColor: '#e6e6e6',
    borderRadius: 20,
    overflow: 'hidden',
    width: '80%', //  reduzir o tamanho do botão
    marginBottom: 5,
  },
});
export default RegisterScreen;
