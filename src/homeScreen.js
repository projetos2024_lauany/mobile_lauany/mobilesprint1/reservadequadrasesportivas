import React from 'react';
import { View, Text, Button, StyleSheet, Image } from 'react-native';

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.Text}>FUTURA PÁGINA HOME</Text>
      <View style={styles.buttonContainer}>
        <Button
          title="SAIR"
          onPress={() => navigation.navigate('PaginaInicial')} color='#3e945a' />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#faf2f2',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 100, // Ajusta as informações para baixo
  },
  Text: {
    alignItems: "center",
    justifyContent: "center",
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 5,
  },
  input: {
    borderColor: '#4dbf72',
    borderWidth: 3,
    borderRadius: 10,
    fontSize: 15,
    width: '80%',
    padding: 10,
    marginVertical: 5,
    paddingHorizontal: 10,
  },
  buttonContainer: {
    borderRadius: 40, // Arredonda o botão
    overflow: 'hidden',
    width: '80%', // Ajusta a largura do botão
    marginBottom: 5,
    backgroundColor: '#e6e6e6',
    
  },
});

export default HomeScreen;

