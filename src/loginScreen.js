import React from "react";
import { View, TextInput, Button, StyleSheet, Text } from "react-native";

const LoginScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.textInput}>INSIRA SUAS INFORMAÇÕES DE LOGIN</Text>
      <TextInput style={styles.input} placeholder="Email" />
      <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true} />
      <View style={styles.buttonContainer}>
        <Button title="LOGIN" onPress={() => navigation.navigate('Home')} color='#3e945a' />
      </View>
      <View style={styles.goBackContainer}>
        <Button title="VOLTAR PARA A PÁGINA INICIAL" onPress={() => navigation.navigate('PaginaInicial')} color='#3e945a' />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#faf2f2',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal:16,
    marginLeft: 10,
    marginRight: 10,
  },
  textInput: {
    height: 20,
    width: 300,
    marginBottom: 16,
    fontSize: 15, // tamanho da letra
    fontWeight: 'bold', // texto em negrito
    marginBottom: 20,
  },
  input: {

    borderColor: '#4dbf72',
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 12,
    width: '80%',
    padding: 10,
    marginVertical: 3,
    marginTop: 3,
    marginBottom: 5,
    paddingHorizontal: 10,
    height: 40,
  },
  buttonContainer: {
    backgroundColor: '#e6e6e6',
    borderRadius: 20,
    overflow: 'hidden',
    width: '80%', // para reduzir o tamanho
    marginBottom: 3,
    marginVertical: 10,
    marginTop: 4,
    
  },
  goBackContainer: {
    backgroundColor: '#e6e6e6',
    borderRadius: 20,
    overflow: 'hidden',
    width: '80%', //  reduzir o tamanho do botão
    marginBottom: 5,
    
  },
});

export default LoginScreen;
